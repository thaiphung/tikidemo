package com.tiki.productdemo.service;

import com.tiki.productdemo.entity.*;
import com.tiki.productdemo.repository.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DataService {

	private static DataService instance;
	private static List<DataType> listDataType = new ArrayList<DataType>();
	private static List<AttributeType> listAttributeType = new ArrayList<AttributeType>();
	private static List<Attribute> listAttribute = new ArrayList<Attribute>();
	private static List<Product> listProduct = new ArrayList<Product>();
	private static List<ProductDetail> listProductDetail = new ArrayList<ProductDetail>();
	private static List<Price> listPrice = new ArrayList<Price>();
	private static List<ImageFile> listImageFile = new ArrayList<ImageFile>();
	private static final String imagePath = "root/img/";
	IBaseRepository repos = BaseRepository.getInstance();

	public static DataService getInstance() {
		if (instance == null) {
			instance = new DataService();
		}
		return instance;
	}

	private DataService() {

	}

	public void createInitData() {

		// Add data DataType
		repos.addDataType(listDataType, new DataType("STRING", "String"));
		repos.addDataType(listDataType, new DataType("DECIMAL", "Decimal"));
		repos.addDataType(listDataType, new DataType("FLOAT", "Float"));
		repos.addDataType(listDataType, new DataType("DATETIME", "DateTime"));
		repos.addDataType(listDataType, new DataType("INTEGER", "Integer"));

		// Add data AttributeType
		repos.addAttributeType(listAttributeType,
				new AttributeType(-1, "color", repos.findDataTypeById(listDataType, "STRING").get(), true, false, true,
						LocalDateTime.now(), LocalDateTime.now(), "ADMIN", "ADMIN"));
		repos.addAttributeType(listAttributeType,
				new AttributeType(-1, "storage", repos.findDataTypeById(listDataType, "STRING").get(), true, false,
						true, LocalDateTime.now(), LocalDateTime.now(), "ADMIN", "ADMIN"));
		repos.addAttributeType(listAttributeType,
				new AttributeType(-1, "description", repos.findDataTypeById(listDataType, "STRING").get(), false, true,
						true, LocalDateTime.now(), LocalDateTime.now(), "ADMIN", "ADMIN"));
		repos.addAttributeType(listAttributeType,
				new AttributeType(-1, "origin", repos.findDataTypeById(listDataType, "STRING").get(), false, false,
						true, LocalDateTime.now(), LocalDateTime.now(), "ADMIN", "ADMIN"));
		repos.addAttributeType(listAttributeType,
				new AttributeType(-1, "factory date", repos.findDataTypeById(listDataType, "DATETIME").get(), false,
						false, true, LocalDateTime.now(), LocalDateTime.now(), "ADMIN", "ADMIN"));
		repos.addAttributeType(listAttributeType,
				new AttributeType(-1, "weight", repos.findDataTypeById(listDataType, "FLOAT").get(), false, false, true,
						LocalDateTime.now(), LocalDateTime.now(), "ADMIN", "ADMIN"));

	}

	public boolean addProduct(String productName, float salePrice, float marketPrice,
			HashMap<String, String> attributes, HashMap<String, String> images) {
		// If have issue on process, we can rollback all or return error step and show
		// to user re-type on this step
		// Add data Product
		Product product = new Product(-1, productName, false, true, LocalDateTime.now(), LocalDateTime.now(), "ADMIN",
				"ADMIN");
		long product_id = repos.addProduct(listProduct, product);

		if (product_id > 0) {
			// product.setId(product_id);
			product = repos.findProductById(listProduct, product_id).get();
		} else {
			return false;
		}

		// add Price for Product
		Price price = new Price(-1, salePrice, marketPrice, true, LocalDateTime.now(), LocalDateTime.now(), "ADMIN",
				"ADMIN");
		long price_id = repos.addPrice(listPrice, price);

		if (price_id == 0) {
			// TODO: rollback Product and log
			return false;
		}

		// add product detail
		ProductDetail productDetail = new ProductDetail(-1, productName, product, price, true, LocalDateTime.now(),
				LocalDateTime.now(), "ADMIN", "ADMIN");
		long product_detail_id = repos.addProductDetail(listProductDetail, productDetail);

		if (product_detail_id > 0) {
			// product.setId(product_id);
			productDetail = repos.findProductDetailById(listProductDetail, product_detail_id).get();
		} else {
			// TODO: rollback all and log
			return false;
		}

		// add Images
		long image_id = 0;
		for (Map.Entry<String, String> element : images.entrySet()) {
			image_id = repos.addImageFile(listImageFile,
					new ImageFile(-1, element.getKey(), imagePath, element.getValue(), productDetail, true, true,
							LocalDateTime.now(), LocalDateTime.now(), "ADMIN", "ADMIN"));
			if (image_id == 0) {
				// TODO: rollback all and log
				return false;
			}
		}

		// add Attribute of this product
		long attribute_id = 0;
		for (Map.Entry<String, String> element : attributes.entrySet()) {
			attribute_id = repos.addAttribute(listAttribute,
					new Attribute(-1, repos.findAttributeTypeByName(listAttributeType, element.getKey()).get(),
							element.getValue(), productDetail,
							1, true, LocalDateTime.now(), LocalDateTime.now(), "ADMIN", "ADMIN"));
			if (attribute_id == 0) {
				// TODO: rollback all and log
				return false;
			}
		}
		

		// update Product Full name is Iphone X 64Gb Black
		repos.updateFullNameOfProductDetailByID(listAttribute, listProductDetail, product_detail_id);
		// config Special Product
		repos.updateSpecialProductById(listProduct, listProductDetail, product_id);

		return true;
	}
	
	public long getProductSize() {
		return listProduct.size();
	}
	
	public long getProductDetailSize() {
		return listProductDetail.size();
	}
	
	public boolean isSpecialProduct(long product_id) {
		return repos.findProductById(listProduct, product_id).get().getIsSpecial();
	}

	
}
