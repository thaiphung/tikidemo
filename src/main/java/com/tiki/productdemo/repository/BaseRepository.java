/**
 * 
 */
package com.tiki.productdemo.repository;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import com.tiki.productdemo.entity.*;

/**
 * 
 * @author Thai Phung
 * @Date Created: Feb 17, 2019
 * @Project TikiDemo
 */
public class BaseRepository implements IBaseRepository {

	private static IBaseRepository instance;
	private static final int CONFIG_SPEC_PRODUCT = 1;

	public static IBaseRepository getInstance() {
		if (instance == null) {
			instance = new BaseRepository();
		}
		return instance;
	}

	private BaseRepository() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tiki.repository.IBaseRepository#findDataTypeById(java.util.List,
	 * java.lang.String)
	 */
	@Override
	public Optional<DataType> findDataTypeById(List<DataType> dataType, String code) {
		return dataType.stream().filter(t -> t.getCode() == code).findFirst();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tiki.repository.IBaseRepository#addDataType(java.util.List,
	 * com.tiki.Entity.DataType)
	 */
	@Override
	public String addDataType(List<DataType> dataType, DataType objDataType) {
		dataType.add(objDataType);
		return objDataType.getCode();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.tiki.repository.IBaseRepository#findAttributeTypeById(java.util.List,
	 * java.lang.long)
	 */
	@Override
	public Optional<AttributeType> findAttributeTypeById(List<AttributeType> attributeType, long id) {
		return attributeType.stream().filter(t -> t.getId() == id).findFirst();
	}

	@Override
	public Optional<AttributeType> findAttributeTypeByName(List<AttributeType> attributeType, String name) {
		return attributeType.stream().filter(t -> t.getName() == name).findFirst();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tiki.repository.IBaseRepository#addAttributeType(java.util.List,
	 * com.tiki.Entity.AttributeType)
	 */
	@Override
	public long addAttributeType(List<AttributeType> attributeType, AttributeType objAttributeType) {
		long nextId = 1L;
		if (attributeType.size() > 0) {
			nextId = attributeType.stream().max(Comparator.comparing(AttributeType::getId)).get().getId() + 1;
		}
		objAttributeType.setId(nextId);
		attributeType.add(objAttributeType);
		return objAttributeType.getId();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tiki.repository.IBaseRepository#findAttributeById(java.util.List,
	 * java.lang.long)
	 */
	@Override
	public Optional<Attribute> findAttributeById(List<Attribute> attribute, long id) {
		return attribute.stream().filter(t -> t.getId() == id).findFirst();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tiki.repository.IBaseRepository#addAttribute(java.util.List,
	 * com.tiki.Entity.Attribute)
	 */
	@Override
	public long addAttribute(List<Attribute> attribute, Attribute objAttribute) {
		long nextId = 1L;
		if (attribute.size() > 0) {
			nextId = attribute.stream().max(Comparator.comparing(Attribute::getId)).get().getId() + 1;
		}
		objAttribute.setId(nextId);
		attribute.add(objAttribute);
		return objAttribute.getId();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tiki.repository.IBaseRepository#findImageFileById(java.util.List,
	 * java.lang.long)
	 */
	@Override
	public Optional<ImageFile> findImageFileById(List<ImageFile> imageFile, long id) {
		return imageFile.stream().filter(t -> t.getId() == id).findFirst();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tiki.repository.IBaseRepository#addImageFile(java.util.List,
	 * com.tiki.Entity.ImageFile)
	 */
	@Override
	public long addImageFile(List<ImageFile> imageFile, ImageFile objImageFile) {
		long nextId = 1L;
		try {
			if (imageFile.size() > 0) {
				nextId = imageFile.stream().max(Comparator.comparing(ImageFile::getId)).get().getId() + 1;
			}
			objImageFile.setId(nextId);
			imageFile.add(objImageFile);
		} catch(Exception ex) {
			nextId = 0;
		}
		
		return nextId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tiki.repository.IBaseRepository#findPriceById(java.util.List,
	 * java.lang.long)
	 */
	@Override
	public Optional<Price> findPriceById(List<Price> price, long id) {
		return price.stream().filter(t -> t.getId() == id).findFirst();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tiki.repository.IBaseRepository#addPrice(java.util.List,
	 * com.tiki.Entity.Price)
	 */
	@Override
	public long addPrice(List<Price> price, Price objPrice) {
		long nextId = 1L;
		try {
			if (price.size() > 0) {
				nextId = price.stream().max(Comparator.comparing(Price::getId)).get().getId() + 1;
			}
			objPrice.setId(nextId);

			price.add(objPrice);
		} catch (Exception ex) {
			nextId = 0;
		}
		return nextId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tiki.repository.IBaseRepository#findProductById(java.util.List,
	 * java.lang.long)
	 */
	@Override
	public Optional<Product> findProductById(List<Product> product, long id) {
		return product.stream().filter(t -> t.getId() == id).findFirst();
	}

	@Override
	public Optional<Product> findProductByName(List<Product> product, String name) {
		return product.stream().filter(t -> t.getName() == name).findFirst();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tiki.repository.IBaseRepository#addProduct(java.util.List,
	 * com.tiki.Entity.Product)
	 */
	@Override
	public long addProduct(List<Product> product, Product objProduct) {
		long nextId = 1L;
		try {
			Optional<Product> prod = findProductByName(product, objProduct.getName());

			if (prod.isEmpty()) {
				if (product.size() > 0) {
					nextId = product.stream().max(Comparator.comparing(Product::getId)).get().getId() + 1;
				}

				objProduct.setId(nextId);
				product.add(objProduct);
			} else {
				nextId = prod.get().getId();
			}
		} catch (Exception ex) {
			nextId = 0;
		}
		return nextId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.tiki.repository.IBaseRepository#findProductDetailById(java.util.List,
	 * java.lang.long)
	 */
	@Override
	public Optional<ProductDetail> findProductDetailById(List<ProductDetail> productDetail, long id) {
		return productDetail.stream().filter(t -> t.getId() == id).findFirst();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tiki.repository.IBaseRepository#addProductDetail(java.util.List,
	 * com.tiki.Entity.ProductDetail)
	 */
	@Override
	public long addProductDetail(List<ProductDetail> productDetail, ProductDetail objProductDetail) {
		long nextId = 1L;
		try {
			if (productDetail.size() > 0) {
				nextId = productDetail.stream().max(Comparator.comparing(ProductDetail::getId)).get().getId() + 1;
			}
			objProductDetail.setId(nextId);

			productDetail.add(objProductDetail);
		} catch (Exception ex) {
			nextId = 0;
		}
		return nextId;
	}

	/**
	 * 
	 * @param productDetail
	 * @param productId
	 * @return
	 */
	@Override
	public long countProductDetailByProductIdIsActive(List<ProductDetail> productDetail, long productId) {
		long numPro = 0;
		if (productDetail.size() > 0) {
			numPro = productDetail.stream().filter(t -> t.getProduct().getId() == productId && t.getIsActive()).count();
		}
		return numPro;
	}

	@Override
	public boolean checkSpecialProduct(List<ProductDetail> productDetail, long productId) {
		return countProductDetailByProductIdIsActive(productDetail, productId) >  CONFIG_SPEC_PRODUCT ? true : false;
	}

	@Override
	public long updateSpecialProductById(List<Product> product, List<ProductDetail> productDetail, long id) {
		Optional<Product> pro = product.stream().filter(t -> t.getId() == id).findFirst();
		if (!pro.isEmpty()) {
			pro.get().setIsSpecial(checkSpecialProduct(productDetail, id));
			return id;
		}

		return -1;
	}

	@Override
	public String getProductFullName(List<Attribute> attribute, long productDetailId) {
		StringBuilder name = new StringBuilder("");
		attribute.stream()
				.filter(t -> t.getProductDetail().getId() == productDetailId && t.getIsActive()
						&& t.getAttributeType().isShowOnName())
				.sorted(Comparator.comparing(Attribute::getSortOrder)).forEach(t -> name.append(" " + t.getValue()));
		return name.toString();

	}

	@Override
	public long updateFullNameOfProductDetailByID(List<Attribute> attribute, List<ProductDetail> productDetail,
			long id) {
		Optional<ProductDetail> pro_detail = Optional.empty();
		if (productDetail.size() > 0) {
			pro_detail = productDetail.stream().filter(t -> t.getId() == id && t.getIsActive()).findFirst();
		}
		if (!pro_detail.isEmpty()) {
			pro_detail.get().setFullName(pro_detail.get().getProduct().getName() + getProductFullName(attribute, id));
			return pro_detail.get().getId();
		}
		return -1;
	}
}
