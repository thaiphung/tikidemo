package com.tiki.productdemo.repository;

import java.util.List;
import java.util.Optional;

import com.tiki.productdemo.entity.Attribute;
import com.tiki.productdemo.entity.AttributeType;
import com.tiki.productdemo.entity.DataType;
import com.tiki.productdemo.entity.ImageFile;
import com.tiki.productdemo.entity.Price;
import com.tiki.productdemo.entity.Product;
import com.tiki.productdemo.entity.ProductDetail;

public interface IBaseRepository {

	Optional<DataType> findDataTypeById(List<DataType> dataType, String code);

	String addDataType(List<DataType> dataType, DataType objDataType);

	Optional<AttributeType> findAttributeTypeById(List<AttributeType> attributeType, long id);

	long addAttributeType(List<AttributeType> attributeType, AttributeType objAttributeType);

	Optional<Attribute> findAttributeById(List<Attribute> attribute, long id);

	long addAttribute(List<Attribute> attribute, Attribute objAttribute);

	Optional<ImageFile> findImageFileById(List<ImageFile> imageFile, long id);

	long addImageFile(List<ImageFile> imageFile, ImageFile objImageFile);

	Optional<Price> findPriceById(List<Price> price, long id);

	long addPrice(List<Price> price, Price objPrice);

	Optional<Product> findProductById(List<Product> product, long id);
	
	Optional<Product> findProductByName(List<Product> product, String name);

	long addProduct(List<Product> product, Product objProduct);

	Optional<ProductDetail> findProductDetailById(List<ProductDetail> productDetail, long id);

	long addProductDetail(List<ProductDetail> productDetail, ProductDetail objProductDetail);
	
	long countProductDetailByProductIdIsActive(List<ProductDetail> productDetail, long productId);
	
	boolean checkSpecialProduct(List<ProductDetail> productDetail, long productId);

	Optional<AttributeType> findAttributeTypeByName(List<AttributeType> attributeType, String name);

	String getProductFullName(List<Attribute> attribute, long productDetailId);

	long updateFullNameOfProductDetailByID(List<Attribute> attribute, List<ProductDetail> productDetail, long id);

	long updateSpecialProductById(List<Product> product, List<ProductDetail> productDetail, long id);

}