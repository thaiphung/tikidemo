package com.tiki.productdemo;

import java.util.HashMap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.tiki.productdemo.service.DataService;

@SpringBootApplication
public class TikiDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(TikiDemoApplication.class, args);
		DataService.getInstance().createInitData();
		
		//add product 1
		HashMap<String, String> attributes = new HashMap<>();
		attributes.put("storage", "64Gb");
		attributes.put("color", "Black");
		attributes.put("description", "iphone X 2018");
		attributes.put("origin", "Apple");
		attributes.put("factory date", "24/04/2018");
		attributes.put("weight", "670g");
		
		HashMap<String, String> images = new HashMap<>();
		images.put("image1", "JPG");
		images.put("image2", "JPG");
		images.put("image3", "JPG");		
		
		DataService.getInstance().addProduct("Iphone X", 999, 1099, attributes, images);
		System.out.println("Add product 1: Iphone  X 64Gb Black: product - " 
				+ DataService.getInstance().getProductSize()
				+ ", product detail: "
				+ DataService.getInstance().getProductDetailSize()
				+ ", isSpecial Product: "
				+ DataService.getInstance().isSpecialProduct(1)
				);
		

		//add product 2
		attributes = new HashMap<>();
		images = new HashMap<>();
		
		attributes.put("storage", "128Gb");
		attributes.put("color", "Yellow");
		attributes.put("description", "iphone X 2018");
		attributes.put("origin", "Apple");
		attributes.put("factory date", "24/04/2018");
		attributes.put("weight", "690g");
		DataService.getInstance().addProduct("Iphone X", 950, 1050, attributes, images);
		System.out.println("Add product 2 - Iphone X 128Gb Yellow: "
				+ DataService.getInstance().getProductSize()
				+ ", product detail: "
				+ DataService.getInstance().getProductDetailSize()
				+ ", isSpecial Product: "
				+ DataService.getInstance().isSpecialProduct(1));
	}

}
