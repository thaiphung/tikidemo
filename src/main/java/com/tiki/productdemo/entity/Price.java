/**
 * 
 */
package com.tiki.productdemo.entity;

import java.time.LocalDateTime;

/**
 * 
 * @author Thai Phung
 * @Date Created: Feb 17, 2019
 * @Project test-Tiki
 */
public class Price {

	private long id;
	private float salePrice;
	private float marketPrice;
	private boolean isActive;
	private LocalDateTime dateCreated;
	private LocalDateTime dateModified;
	private String userCreated;
	private String userModified;
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the salePrice
	 */
	public float getSalePrice() {
		return salePrice;
	}
	/**
	 * @param salePrice the salePrice to set
	 */
	public void setSalePrice(float salePrice) {
		this.salePrice = salePrice;
	}
	/**
	 * @return the marketPrice
	 */
	public float getMarketPrice() {
		return marketPrice;
	}
	/**
	 * @param marketPrice the marketPrice to set
	 */
	public void setMarketPrice(float marketPrice) {
		this.marketPrice = marketPrice;
	}
	/**
	 * @return the isActive
	 */
	public boolean getIsActive() {
		return isActive;
	}
	/**
	 * @param isActive the isActive to set
	 */
	public void setIsActive(boolean isActive) {
		this.isActive = isActive;
	}
	/**
	 * @return the dateCreated
	 */
	public LocalDateTime getDateCreated() {
		return dateCreated;
	}
	/**
	 * @param dateCreated the dateCreated to set
	 */
	public void setDateCreated(LocalDateTime dateCreated) {
		this.dateCreated = dateCreated;
	}
	/**
	 * @return the dateModified
	 */
	public LocalDateTime getDateModified() {
		return dateModified;
	}
	/**
	 * @param dateModified the dateModified to set
	 */
	public void setDateModified(LocalDateTime dateModified) {
		this.dateModified = dateModified;
	}
	/**
	 * @return the userCreated
	 */
	public String getUserCreated() {
		return userCreated;
	}
	/**
	 * @param userCreated the userCreated to set
	 */
	public void setUserCreated(String userCreated) {
		this.userCreated = userCreated;
	}
	/**
	 * @return the userModified
	 */
	public String getUserModified() {
		return userModified;
	}
	/**
	 * @param userModified the userModified to set
	 */
	public void setUserModified(String userModified) {
		this.userModified = userModified;
	}
	public Price(long id, float salePrice, float marketPrice, boolean isActive, LocalDateTime dateCreated,
			LocalDateTime dateModified, String userCreated, String userModified) {
		super();
		this.id = id;
		this.salePrice = salePrice;
		this.marketPrice = marketPrice;
		this.isActive = isActive;
		this.dateCreated = dateCreated;
		this.dateModified = dateModified;
		this.userCreated = userCreated;
		this.userModified = userModified;
	}
	public Price() {
		super();
	}
	
	
}
