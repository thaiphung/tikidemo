/**
 * 
 */
package com.tiki.productdemo.entity;

/**
 * 
 * @author Thai Phung
 * @Date Created: Feb 17, 2019
 * @Project test-Tiki
 */
public class DataType {
	private String code;
	private String description;
	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	public DataType(String code, String description) {
		super();
		this.code = code;
		this.description = description;
	}
	public DataType() {
		super();
	}
	
	
}
