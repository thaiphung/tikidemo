/**
 * 
 */
package com.tiki.productdemo.entity;

import java.time.LocalDateTime;

/**
 * 
 * @author Thai Phung
 * @Date Created: Feb 17, 2019
 * @Project test-Tiki
 */
public class ImageFile {

	private long id;
	private String fileName;
	private String url;
	private String fileType;
	private boolean isDefault;
	private ProductDetail productDetail;
	private boolean isActive;
	private LocalDateTime dateCreated;
	private LocalDateTime dateModified;
	private String userCreated;
	private String userModified;
	
	public ImageFile(long id, String fileName, String url, String fileType, ProductDetail productDetail, boolean isDefault,
			boolean isActive, LocalDateTime dateCreated, LocalDateTime dateModified, String userCreated, String userModified) {
		super();
		this.id = id;
		this.fileName = fileName;
		this.url = url;
		this.fileType = fileType;
		this.productDetail = productDetail;
		this.isDefault = isDefault;
		this.isActive = isActive;
		this.dateCreated = dateCreated;
		this.dateModified = dateModified;
		this.userCreated = userCreated;
		this.userModified = userModified;
	}

	public ImageFile() {
		super();
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the fileType
	 */
	public String getFileType() {
		return fileType;
	}

	/**
	 * @param fileType the fileType to set
	 */
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	/**
	 * @return the productDetail
	 */
	public ProductDetail getProductDetail() {
		return productDetail;
	}

	/**
	 * @param productDetail the productDetail to set
	 */
	public void setProductDetail(ProductDetail productDetail) {
		this.productDetail = productDetail;
	}

	/**
	 * @return the isActive
	 */
	public boolean getIsActive() {
		return isActive;
	}

	/**
	 * @param isActive the isActive to set
	 */
	public void setIsActive(boolean isActive) {
		this.isActive = isActive;
	}

	/**
	 * @return the dateCreated
	 */
	public LocalDateTime getDateCreated() {
		return dateCreated;
	}

	/**
	 * @param dateCreated the dateCreated to set
	 */
	public void setDateCreated(LocalDateTime dateCreated) {
		this.dateCreated = dateCreated;
	}

	/**
	 * @return the dateModified
	 */
	public LocalDateTime getDateModified() {
		return dateModified;
	}

	/**
	 * @param dateModified the dateModified to set
	 */
	public void setDateModified(LocalDateTime dateModified) {
		this.dateModified = dateModified;
	}

	/**
	 * @return the userCreated
	 */
	public String getUserCreated() {
		return userCreated;
	}

	/**
	 * @param userCreated the userCreated to set
	 */
	public void setUserCreated(String userCreated) {
		this.userCreated = userCreated;
	}

	/**
	 * @return the userModified
	 */
	public String getUserModified() {
		return userModified;
	}

	/**
	 * @param userModified the userModified to set
	 */
	public void setUserModified(String userModified) {
		this.userModified = userModified;
	}

	/**
	 * @return the isDefault
	 */
	public boolean getIsDefault() {
		return isDefault;
	}

	/**
	 * @param isDefault the isDefault to set
	 */
	public void setIsDefault(boolean isDefault) {
		this.isDefault = isDefault;
	}
	
	
	
}
