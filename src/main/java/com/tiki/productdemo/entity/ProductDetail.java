package com.tiki.productdemo.entity;

import java.time.LocalDateTime;

/**
 * 
 * @author Thai Phung
 * @Date Created: Feb 17, 2019
 * @Project test-Tiki
 */
public class ProductDetail {

	private long id;
	private String fullName;
	private Product product;
	private Price price;
	private boolean isActive;
	private LocalDateTime dateCreated;
	private LocalDateTime dateModified;
	private String userCreated;
	private String userModified;
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the product
	 */
	public Product getProduct() {
		return product;
	}
	/**
	 * @param product the product to set
	 */
	public void setProduct(Product product) {
		this.product = product;
	}
	/**
	 * @return the price
	 */
	public Price getPrice() {
		return price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(Price price) {
		this.price = price;
	}
	/**
	 * @return the isActive
	 */
	public boolean getIsActive() {
		return isActive;
	}
	/**
	 * @param isActive the isActive to set
	 */
	public void setIsActive(boolean isActive) {
		this.isActive = isActive;
	}
	/**
	 * @return the dateCreated
	 */
	public LocalDateTime getDateCreated() {
		return dateCreated;
	}
	/**
	 * @param dateCreated the dateCreated to set
	 */
	public void setDateCreated(LocalDateTime dateCreated) {
		this.dateCreated = dateCreated;
	}
	/**
	 * @return the dateModified
	 */
	public LocalDateTime getDateModified() {
		return dateModified;
	}
	/**
	 * @param dateModified the dateModified to set
	 */
	public void setDateModified(LocalDateTime dateModified) {
		this.dateModified = dateModified;
	}
	/**
	 * @return the userCreated
	 */
	public String getUserCreated() {
		return userCreated;
	}
	/**
	 * @param userCreated the userCreated to set
	 */
	public void setUserCreated(String userCreated) {
		this.userCreated = userCreated;
	}
	/**
	 * @return the userModified
	 */
	public String getUserModified() {
		return userModified;
	}
	/**
	 * @param userModified the userModified to set
	 */
	public void setUserModified(String userModified) {
		this.userModified = userModified;
	}
	
	
	/**
	 * @return the fullName
	 */
	public String getFullName() {
		return fullName;
	}
	/**
	 * @param fullName the fullName to set
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	/**
	 * @param isActive the isActive to set
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	
	
	public ProductDetail(long id, String fullName, Product product, Price price, boolean isActive,
			LocalDateTime dateCreated, LocalDateTime dateModified, String userCreated, String userModified) {
		super();
		this.id = id;
		this.fullName = fullName;
		this.product = product;
		this.price = price;
		this.isActive = isActive;
		this.dateCreated = dateCreated;
		this.dateModified = dateModified;
		this.userCreated = userCreated;
		this.userModified = userModified;
	}
	public ProductDetail() {
		super();
	}
	
	
}
