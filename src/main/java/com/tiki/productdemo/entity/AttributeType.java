package com.tiki.productdemo.entity;

import java.time.LocalDateTime;

/**
 * 
 * @author Thai Phung
 * @Date Created: Feb 17, 2019
 * @Project test-Tiki
 */
public class AttributeType {
	private long id;
	private String name;
	private DataType dataType;
	private boolean isShowOnName;
	private boolean isRequired;
	private boolean isActive;
	private LocalDateTime dateCreated;
	private LocalDateTime dateModified;
	private String userCreated;
	private String userModified;
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the dataTypeCode
	 */
	public DataType getDataTypeCode() {
		return dataType;
	}
	/**
	 * @param dataTypeCode the dataTypeCode to set
	 */
	public void setDataTypeCode(DataType dataType) {
		this.dataType = dataType;
	}
	/**
	 * @return the isActive
	 */
	public boolean getIsActive() {
		return isActive;
	}
	/**
	 * @param isActive the isActive to set
	 */
	public void setIsActive(boolean isActive) {
		this.isActive = isActive;
	}
	/**
	 * @return the dateCreated
	 */
	public LocalDateTime getDateCreated() {
		return dateCreated;
	}
	/**
	 * @param dateCreated the dateCreated to set
	 */
	public void setDateCreated(LocalDateTime dateCreated) {
		this.dateCreated = dateCreated;
	}
	/**
	 * @return the dateModified
	 */
	public LocalDateTime getDateModified() {
		return dateModified;
	}
	/**
	 * @param dateModified the dateModified to set
	 */
	public void setDateModified(LocalDateTime dateModified) {
		this.dateModified = dateModified;
	}
	/**
	 * @return the userCreated
	 */
	public String getUserCreated() {
		return userCreated;
	}
	/**
	 * @param userCreated the userCreated to set
	 */
	public void setUserCreated(String userCreated) {
		this.userCreated = userCreated;
	}
	/**
	 * @return the userModified
	 */
	public String getUserModified() {
		return userModified;
	}
	/**
	 * @param userModified the userModified to set
	 */
	public void setUserModified(String userModified) {
		this.userModified = userModified;
	}
	
	
	/**
	 * @return the isRequired
	 */
	public boolean getIsRequired() {
		return isRequired;
	}
	/**
	 * @param isRequired the isRequired to set
	 */
	public void setIsRequired(boolean isRequired) {
		this.isRequired = isRequired;
	}
	
	
	/**
	 * @return the dataType
	 */
	public DataType getDataType() {
		return dataType;
	}
	/**
	 * @param dataType the dataType to set
	 */
	public void setDataType(DataType dataType) {
		this.dataType = dataType;
	}
	/**
	 * @return the isShowOnName
	 */
	public boolean isShowOnName() {
		return isShowOnName;
	}
	/**
	 * @param isShowOnName the isShowOnName to set
	 */
	public void setShowOnName(boolean isShowOnName) {
		this.isShowOnName = isShowOnName;
	}
	/**
	 * @param isRequired the isRequired to set
	 */
	public void setRequired(boolean isRequired) {
		this.isRequired = isRequired;
	}
	/**
	 * @param isActive the isActive to set
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	
	
	public AttributeType(long id, String name, DataType dataType, boolean isShowOnName, boolean isRequired,
			boolean isActive, LocalDateTime dateCreated, LocalDateTime dateModified, String userCreated,
			String userModified) {
		super();
		this.id = id;
		this.name = name;
		this.dataType = dataType;
		this.isShowOnName = isShowOnName;
		this.isRequired = isRequired;
		this.isActive = isActive;
		this.dateCreated = dateCreated;
		this.dateModified = dateModified;
		this.userCreated = userCreated;
		this.userModified = userModified;
	}
	public AttributeType() {
		super();
	}

	
}
