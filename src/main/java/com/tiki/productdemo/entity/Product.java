/**
 *
 */
package com.tiki.productdemo.entity;

import java.time.LocalDateTime;

/**
 * 
 * @author Thai Phung
 * @Date Created: Feb 17, 2019
 * @Project test-Tiki
 */
public class Product {

	private long id;
	private String name;
	private boolean isSpecial;
	private boolean isActive;
	private LocalDateTime dateCreated;
	private LocalDateTime dateModified;
	private String userCreated;
	private String userModified;
	//private List<ProductDetail> productDetail;
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the isSpecial
	 */
	public boolean getIsSpecial() {
		return isSpecial;
	}
	/**
	 * @param isSpecial the isSpecial to set
	 */
	public void setIsSpecial(boolean isSpecial) {
		this.isSpecial = isSpecial;
	}
	/**
	 * @return the isActive
	 */
	public boolean getIsActive() {
		return isActive;
	}
	/**
	 * @param isActive the isActive to set
	 */
	public void setIsActive(boolean isActive) {
		this.isActive = isActive;
	}
	/**
	 * @return the dateCreated
	 */
	public LocalDateTime getDateCreated() {
		return dateCreated;
	}
	/**
	 * @param dateCreated the dateCreated to set
	 */
	public void setDateCreated(LocalDateTime dateCreated) {
		this.dateCreated = dateCreated;
	}
	/**
	 * @return the dateModified
	 */
	public LocalDateTime getDateModified() {
		return dateModified;
	}
	/**
	 * @param dateModified the dateModified to set
	 */
	public void setDateModified(LocalDateTime dateModified) {
		this.dateModified = dateModified;
	}
	/**
	 * @return the userCreated
	 */
	public String getUserCreated() {
		return userCreated;
	}
	/**
	 * @param userCreated the userCreated to set
	 */
	public void setUserCreated(String userCreated) {
		this.userCreated = userCreated;
	}
	/**
	 * @return the userModified
	 */
	public String getUserModified() {
		return userModified;
	}
	/**
	 * @param userModified the userModified to set
	 */
	public void setUserModified(String userModified) {
		this.userModified = userModified;
	}

	/**
	 * @return the productDetail
	 */
//	public List<ProductDetail> getProductDetail() {
//		return productDetail;
//	}
//	/**
//	 * @param productDetail the productDetail to set
//	 */
//	public void setProductDetail(List<ProductDetail> productDetail) {
//		this.productDetail = productDetail;
//	}


	public Product(long id, String name, boolean isSpecial, boolean isActive, LocalDateTime dateCreated,
			LocalDateTime dateModified, String userCreated, String userModified) {
		super();
		this.id = id;
		this.name = name;
		this.isSpecial = isSpecial;
		this.isActive = isActive;
		this.dateCreated = dateCreated;
		this.dateModified = dateModified;
		this.userCreated = userCreated;
		this.userModified = userModified;
		//this.productDetail = productDetail;
	}
	public Product() {
		super();
	}


}
