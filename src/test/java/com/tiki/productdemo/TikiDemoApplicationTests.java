package com.tiki.productdemo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.tiki.productdemo.service.DataService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=TikiDemoApplication.class)
public class TikiDemoApplicationTests {

	@BeforeClass
	public static void setUp() {
		DataService.getInstance().createInitData();

	}

	@Test
	public void addProduct1() {
		// add product 1
		HashMap<String, String> attributes = new HashMap<>();
		attributes.put("storage", "64Gb");
		attributes.put("color", "Black");
		attributes.put("description", "iphone X 2018");
		attributes.put("origin", "Apple");
		attributes.put("factory date", "24/04/2018");
		attributes.put("weight", "670g");

		HashMap<String, String> images = new HashMap<>();
		images.put("image1", "JPG");
		images.put("image2", "JPG");
		images.put("image3", "JPG");
		
		DataService.getInstance().addProduct("Iphone X", 999, 1099, attributes, images);
		
		System.out.println("Add product 1: Iphone X 64Gb Black");
		
		assertEquals(1, DataService.getInstance().getProductSize());
		assertEquals(1, DataService.getInstance().getProductDetailSize());
		//assertFalse(DataService.getInstance().isSpecialProduct(1));

	}	

	@Test
	public void addProduct2() {
		// add product 2
		HashMap<String, String> attributes = new HashMap<>();
		HashMap<String, String> images = new HashMap<>();

		attributes.put("storage", "128Gb");
		attributes.put("color", "Yellow");
		attributes.put("description", "iphone X 2018");
		attributes.put("origin", "Apple");
		attributes.put("factory date", "24/04/2018");
		attributes.put("weight", "690g");
		DataService.getInstance().addProduct("Iphone X", 950, 1050, attributes, images);
		
		System.out.println("Add product 2: Iphone X 128Gb Yellow");
		
		assertEquals(1, DataService.getInstance().getProductSize());
		assertEquals(2, DataService.getInstance().getProductDetailSize());
		assertTrue(DataService.getInstance().isSpecialProduct(1));
	}
	
	@Test
	public void checkSpecialProduct2() {
		assertTrue(DataService.getInstance().isSpecialProduct(1));
	}

}
